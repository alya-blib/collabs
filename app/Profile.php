<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'bio', 'age', 'address', 'users_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }
}
