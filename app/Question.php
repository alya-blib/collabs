<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = "questions";
    protected $fillable = ['title','content','img', "genres_id"];
    

    public function genre()
    {
        return $this->belongsTo('App\Genre', 'genres_id');
    }

    public function answer()
    {
        return $this->hasMany('App\Answer', 'id');
    }
    // protected $guarded = [];
}
