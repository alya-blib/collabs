<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;
use App\Question;
use App\User;
use Illuminate\Support\Facades\Auth;


class ProfileController extends Controller
{

        public function index() {
            $profile = Profile::where('users_id', Auth::id())->first();
            
            return view('profile.edit', compact('profile'));
            
        }

     
        // public function edit($id) {
        //     // $cast = DB::table('cast')->where('id', $id)->first();
        //     $user = User::get();
        //     $profile = Question::find($id);
        //     return view('profile.edit', compact('profile', 'user'));
        // }

        public function update($id, Request $request){

            
            $request->validate([
                'bio' => 'required',
                'age' => 'required',
                'address' => 'required',
                
            ]);
       
        
            $profile = Profile::find($id);

            $profile->bio = $request->bio;
            $profile->age = $request->age;
            $profile->address = $request->address;
            $profile->users_id = Auth::id();

            $profile->save();

            return redirect('/profile');
        }

        public function destroy($id){
            // $query = DB::table('cast')->where('id', $id)->delete();
            Profile::destroy($id);
            return redirect('/profile')->with('success', 'Answer Berhasil Dihapus!');
        }
}
