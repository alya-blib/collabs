<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Answer;
use App\Question;
use Illuminate\Support\Facades\Auth;

class AnswerController extends Controller
{
    public function create(){
      
    }

    public function store(Request $request){
        $request->validate([
            'answer' => 'required',
        ]);
    

      
         $answer = new Answer;
         $answer->users_id = Auth::id();
         $answer->questions_id = $request->questions_id;
         $answer->answer = $request->answer;
         

         $answer->save();

         return redirect()->back();
        }

        public function index() {
        
        }

        public function show($id){
            
        }

        public function edit($id) {
            
        }

        // public function update($id, Request $request){
           
        // }

        public function destroy($id){
          
        }
}
