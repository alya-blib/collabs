<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = "answers";
    protected $fillable = ['answer', 'questions_id'];


    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }

    public function question()
    {
        return $this->belongsTo('App\Question', 'questions_id');
    }
}
