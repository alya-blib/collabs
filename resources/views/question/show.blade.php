@extends ('adminlte.master')

@section('title')
  Detail Question {{$question->title}}
@endsection

@section('content')

<div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Question</h3>
              </div>

<div class="row">
  <div class="col-12 m-3">
      <img src="{{asset('gambar/'.$question->img)}}" width="200px">
      <div class="card-body">
        <h5 class="card-title">{{$question->title}}</h5>
        <p class="card-text">{{$question->content}}</p>
     </div>
  </div>
</div>


  
@forelse ($question->answer as $item)
      <div class="col-12">
      <div class="card">
      <div class="card-body">
        <h5>{{$item->user->name}}</h5>
        <small>{{$item->created_at}}</small>
        <h6 class="card-text">{{$item->answer}}</h6>
      </div>
      </div>
    </div>
</div>
  @empty
  
  @endforelse

@auth

  <div class="col-9">
  <form role="form" action="/answer" method="POST" enctype="multipart/form-data">
                @csrf
    
                  <div class="form-group">
                    <input type="hidden" value="{{$question->id}}" name="questions_id">
                    <label for="answer">Answer</label>
                    <textarea type="text" class="form-control" id="answer" name="answer" placeholder="Masukkan Komentar"></textarea>
                    @error('answer')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                <div class="card-4">
                  <button type="submit" class="btn btn-primary m-3">Tambah Komentar</button>
                  <a class="btn btn-primary btn-sm" style="inline-block">Kembali</a>
                </div>
              </form>
  </div>

@endauth

</div>
</div>

@endsection