@extends ('adminlte.master')

@section ('content')
<div class="m-3">
<div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                   
                    <!-- Post -->
                    @foreach ($question as $object)
                    <div class="post clearfix">
                      <div class="user">
                        <span class="username">
                          <a href="/profile">{{Auth::user()->name}}</a>
                          <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                        </span>
                      </div>
                      <!-- /.user-block -->
                   
                    <ul>
                        <li>
                        <img src="{{asset('gambar/'.$object->img)}}" class="card-img-top" alt="...">
                        </li>
                        <li>
                            {{$object->content}}
                        </li>
                    </ul>
                 
                    </div>
                    <!-- /.post -->
                
                    @endforelse
            

                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
</div>
</div>

@endsection