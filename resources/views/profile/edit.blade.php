@extends('adminlte.master')
@section ('judul')
  Edit Profile 
@endsection
@section('content')
<div class="ml-3 mt-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Profile</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/profile/{{$profile->id}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group m-2">
                    <label for="bio">User Name</label>
                    <input type="text" class="form-control" value="{{$profile->user->name}} " disabled>
                </div>

                <div class="form-group m-2">
                    <label for="bio">Email</label>
                    <input type="text" class="form-control" value="{{$profile->user->email}} " disabled>
                </div>

                <div class="form-group m-2">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name="bio" value="{{$profile->bio}} " placeholder="Enter Bio">
                    @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group m-2">
                    <label>Age</label>
                    <input type="number" class="form-control" value="{{$profile->age}}">
                 </div>
                
                <div class="form-group m-2">
                    <label>Address</label>
                    <textarea class="form-control" value="{{$profile->address}}"></textarea>
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>

</div>

@endsection
