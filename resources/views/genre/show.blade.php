@extends ('adminlte.master')

@section('content')


<div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Genre Table</h3>
              </div>
<h1 class="m-2">{{$genre->name}}</h1>
<p class="m-2">{{$genre->description}}</p>

<div class="row">
  @forelse ($genre->question as $item)
  <div class="col-4 m-3">
    <div class="card">
      <img src="{{asset('gambar/'.$item->img)}}" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">{{$item->title}}</h5>
        <span class="badge badge-info m-3 mt-3">{{$item->genre->name}}</span>
        <p class="card-text">{{Str::limit($item->content, 20)}}</p>
       
       <form action="/question/{{$item->id}}" method="POST">
         @method('DELETE')
         @csrf
         <a href="/question/{{$item->id}}" class="btn btn-primary btn-sm">Read more</a>
       </form> 
       
      </div>
  </div>
</div>
@empty
<h5 class="m-3">Tidak Ada Question</h5>
@endforelse
</div>
</div>
</div>

@endsection