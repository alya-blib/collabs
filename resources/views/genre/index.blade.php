@extends ('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Genre Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                  @endif
                  <a class="btn btn-primary mb-2" href="/genre/create">Create New Genre</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   @forelse($genre as $key => $genre)
                    <tr>
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $genre->name }} </td>
                        <td> {{ $genre->description }} </td>
                    
                        <td style="display: flex;">
                            <a href="/genre/{{ $genre->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/genre/{{ $genre->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                           <form action="/genre/{{ $genre->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                           </form>
                        
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">No Genre</td>
                    </tr>
                    
                   @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
          
            </div>
    </div>
@endsection